#ifndef _VELU_H_
#define _VELU_H_

#include "../EllipticCurves/models.h"
#include "../EllipticCurves/memory.h"
#include "../EllipticCurves/arithmetic.h"
#include "../Polynomials/multieval.h"

void _init_lengths(unsigned int *, unsigned int *, unsigned int *, unsigned int);
void _F0pF1pF2_F0mF1pF2(fq_poly_t *, fq_poly_t *, MG_point_t, const fq_ctx_t);

void KPS(MG_point_t *, MG_point_t *, MG_point_t *, MG_point_t, unsigned int, unsigned int, unsigned int, unsigned int);
void xISOG(fq_t *, MG_point_t, unsigned int, MG_point_t *, MG_point_t *, MG_point_t *, unsigned int, unsigned int, unsigned int);

void isogeny_from_torsion(fq_t *, MG_point_t, unsigned int);

#endif

