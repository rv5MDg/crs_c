# CRS_C

Isogeny-based Diffie-Hellman protocol.

## Getting started

An example is available in /example/exchange/.
A general presentation of the protocol and experimental results can be found in /presentation.

To compile it use the compile.sh script. 
Flag -DVERBOSE activates additional outputs.
Flag -DTIMING outputs json-style timings for optimization with optimization/optimize.py.

## Authors and acknowledgment
Hugo Nartz, Clement Jacquot

Project mainly based on [this paper](https://hal.inria.fr/hal-01872817/)

## License
GNU/GPL

