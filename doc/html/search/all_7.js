var searchData=
[
  ['remaindercell_0',['remainderCell',['../multieval_8c.html#a424ab9d2b6d1a697269afe75e39b38a8',1,'remainderCell(fq_poly_bcell_t *rop, fq_t *roots, unsigned int offset_start, unsigned int offset_end, const fq_ctx_t *F):&#160;multieval.c'],['../multieval_8h.html#a2ebc27408dca597f79fe56bcdced0408',1,'remainderCell(fq_poly_bcell_t *, fq_t *, unsigned int, unsigned int, const fq_ctx_t *):&#160;multieval.c']]],
  ['remaindertree_1',['remainderTree',['../multieval_8c.html#a3801f215ae6a0dd134a49d3d2f310ceb',1,'remainderTree(fq_poly_btree_t *T, fq_t *roots, unsigned int len, const fq_ctx_t *F):&#160;multieval.c'],['../multieval_8h.html#a5360e6b2ff69747bf4bee5b19c9436d5',1,'remainderTree(fq_poly_btree_t *, fq_t *, unsigned int, const fq_ctx_t *):&#160;multieval.c']]],
  ['roots_2eh_2',['roots.h',['../roots_8h.html',1,'']]]
];
